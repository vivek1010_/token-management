import React from 'react';
import SlashLog from './SlashLog';
import { Card, CardBody, CardHeader, Col, Row, Input, Button} from 'reactstrap';
import {withRouter} from 'react-router-dom';

class TokenManagement extends React.Component{

    constructor(props){
        
        super(props)
         this.state={
             isSubmit:false,
             radioValue:'',
             data:[ 
             { id:1,
             slashUser: 'agyan1',
             slashProject:'Project1',
             slashToken: 'ffcf9a9f719168963a51b461cc0bdab4cd0ba6f4b39ef8ff69367c3e0dd428', 
             slashEnv:'gold', 
             slashTokenCreaonDate:'Apr 04, 2019',
             slashTokenExpiraonDate:'Jun 03, 2019'
            },
             { 
                id:2,
                slashUser: 'agyan2',
                slashProject:'Project1',
                slashToken: 'ffcf9a9f719168963a51b461cc0bdab4cd0ba6f4b39ef8ff69367c3e0dd428', 
                slashEnv:'gold', 
                slashTokenCreaonDate:'Apr 04, 2019',
                slashTokenExpiraonDate:'Jun 03, 2019'
             },
             { 
                id:3,
                slashUser: 'agyan3',
                slashProject:'Project1',
                slashToken: 'ffcf9a9f719168963a51b461cc0bdab4cd0ba6f4b39ef8ff69367c3e0dd428', 
                slashEnv:'gold', 
                slashTokenCreaonDate:'Apr 04, 2019',
                slashTokenExpiraonDate:'Jun 03, 2019'
             },
             {
                id:4,
                slashUser: 'agyan4',
                slashProject:'Project1',
                slashToken: 'ffcf9a9f719168963a51b461cc0bdab4cd0ba6f4b39ef8ff69367c3e0dd428', 
                slashEnv:'gold', 
                slashTokenCreaonDate:'Apr 04, 2019',
                slashTokenExpiraonDate:'Jun 03, 2019'
              }]
         }   
        }
        goBack=()=>{
            this.setState({radioValue:'',isSubmit:false})
        }
        logout=()=>{
            sessionStorage.setItem("loggedIn" , "")
            this.props.history.push('/login');
           
        }
        onchange=(e)=>{
            this.setState({radioValue:e.target.value})
        }
        formSubmit=(e)=>{
            let radioVal=this.state.radioValue;
            if(radioVal == ''){
                alert('Please Select Token to Proceed');
                return false;
            }
           this.setState({isSubmit:true})
		//this.props.history.push('/charts');
        }
        render() {
           const isSubmit=this.state.isSubmit;
            const listItems = this.state.data.map((data) =>
                <tr key={data.id}>
                <td><input type="radio" name="radio" value={data.slashToken} onChange={this.onchange}/></td>
                <td>{data.slashUser}</td>
                <td>{data.slashProject}</td>
                <td>{data.slashToken}</td>
                <td>{data.slashEnv}</td>
                </tr> 
            );
            return (
                 <React.Fragment >
              
                
                 <div className="row">
                 <div className="col-md-4 col-sm-4"></div>
                 </div>
                  {isSubmit ? 
                  (
                      <div >
                    <div className="background">
                    <div className="row header">
                    <div className="col-md-11">Slash[/] Log Search : {this.state.radioValue}</div>
                    <div className="col-md-1"><Button className="pinkButton" onClick={this.logout}>Logout</Button></div>
                    </div>
                    <div className="row subheader">
                    <div className="col-sm-2 col-sm-2">
                    <div>
                    <label className="label">Time</label>
                    <div>
                    <select className="selcls" style={{margin:10}}>
                    <option value="today">Today</option>
                    <option value="Yesterday">Yesterday</option>
                    <option value="week">One Week</option>
                    <option value="month">Month</option>
                    </select>   
                 </div> 
                 </div>
                 </div>
                 <div className="col-md-4 col-sm-2">
                 <label className="label">Log Level</label>
                 
                  <div>
                 
                <input  type="checkbox"  value="fatal"/>
                <label className="sub">Fatal</label>
               
                <input type="checkbox"  value="error"/>
                <label className="sub">Error</label>
                
                <input type="checkbox"  value="warn"/>
                <label className="sub">Warn</label>
                </div>
                     <div>
                     
                     <input type="checkbox"  value="info"/> 
                     <label className="sub">Info</label>
                     
                     <input type="checkbox"  value="debug"/>
                     <label className="sub">Debug</label>
                     </div>
                    
                 </div>
                 <div className="col-md-2 col-sm-12">
                 <label className="label">Environment</label>
                 <select className="selcls" style={{margin:10}}>
                <option value="Silver">Silver</option>
                <option value="Gold">Gold</option>
                <option value="Platinum">Platinum</option>
                </select>
                 </div>
                 <div className="col-md-2 col-sm-12">
                 <label className="label">Host</label>
                 <div>
                 <select className="selcls" style={{margin:10}}>
                <option value="any">Any</option>
                <option value="Host1">Host1</option>
                <option value="Host2">Host2</option>
                <option value="Host3">Host3</option>
                </select>    
                </div>
                 </div>
                 <div className="col-md-2 col-sm-12">
                 <label className="label">User</label>
                 <div>
                 <select className="selcls" style={{margin:10}}>
                <option value="User1">User1</option>
                <option value="User2">User2</option>
                <option value="User3">User3</option>
                <option value="User4">User4</option>
                </select>
                 </div>
                 </div>
                 
                 </div>
                 <div className="row refresh">
                 <div className="col-md-6 col-sm-6">
                 <label style={{margin:10}}><b>Log Message Contains</b></label><input type="text" placeholder="Search Words" />
                 <Button className="pinkButton" style={{margin:10}}>Refresh</Button>
                 </div>
                 </div>
                 
                 </div>

                    <Button color="link" style={{padding:10}} onClick={this.goBack}>Go Back</Button>
                   <SlashLog isSubmit={this.state.isSubmit} token={this.state.radioValue} />

                      </div>
                  
                  )
                   :
                   <div>
                        <div className="row header">
                    <div className="col-md-11">Token Management</div>
                    <div className="col-md-1" ><Button className="pinkButton" onClick={this.logout}>Logout</Button></div>
                        </div>
                <table className="table table-striped">
                 <thead>
                  <tr>
                    <th></th>
                    <th>User</th>
                    <th>Project</th>
                    <th>Token</th>
                    <th>Environment</th>
                  </tr>
                </thead>
                     <tbody>
                     {listItems}
                </tbody>
                
                  </table>
                <div style={{textAlign:"center"}}>
                 
                 <Button  onClick={this.formSubmit}>Submit</Button>
                 </div>
                  
                   </div>
                  }
                  
                 
              </React.Fragment>
            );
        }
    
}

export default withRouter(TokenManagement);