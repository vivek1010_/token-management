import React from 'react';
import { Line} from 'react-chartjs-2';
import { getColor } from 'utils/colors';
import { randomNum } from 'utils/demos';
const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
const genLineData = (moreData = {}, moreData2 = {}) => {
    return {
      labels: MONTHS,
      datasets: [
        {
          label: 'Dataset 1',
          backgroundColor: getColor('primary'),
          borderColor: getColor('primary'),
          borderWidth: 1,
          data: [
            randomNum(),
            randomNum(),
            randomNum(),
            randomNum(),
            randomNum(),
            randomNum(),
            randomNum(),
          ],
          ...moreData,
        },
        {
          label: 'Dataset 2',
          backgroundColor: getColor('secondary'),
          borderColor: getColor('secondary'),
          borderWidth: 1,
          data: [
            randomNum(),
            randomNum(),
            randomNum(),
            randomNum(),
            randomNum(),
            randomNum(),
            randomNum(),
          ],
          ...moreData2,
        },
      ],
    };
  };

  class SlashLog extends React.Component{

    constructor(props){
        super(props)
       
    }

    render(){
        
        return(
            <React.Fragment>
                <div className="slashLog">
                <Line data={genLineData({ fill: false }, { fill: false })} />
                </div>
            
            </React.Fragment>
        );
    }
  }

  export default SlashLog;