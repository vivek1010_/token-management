import React from 'react';
import { Route, Redirect } from 'react-router-dom';


const LayoutRoute = ({ component: Component, layout: Layout, ...rest }) =>{
  const isLoggedIn=sessionStorage.getItem("loggedIn");
  console.log(isLoggedIn);
  var pathLogin=false;
  const path=rest.path;
  if(path == "/login"){
    pathLogin=true;
  }

 return (
  <React.Fragment>
{pathLogin ?(
  <Route
  {...rest}
  render={props => (
    
    <Layout> 
    <Component {...props} />
    </Layout>
   
  )}
/>

):(

  <Route
    {...rest}
    render={props => (
      isLoggedIn ?
      <Layout> 
      <Component {...props} />
      </Layout>
      :
      <Redirect to="/login" />
    )}
  />

)}

  </React.Fragment>
  
);
} 

export default LayoutRoute;
